# 学生管理系统

#### 介绍
入门版 |
适合初学者的学生管理系统，使用了较为简单的页面，前后端一体。主要技术spring+springboot+maven+mysql，项目下载下来只需要改一下配置文件中数据库的账号密码，数据库直接在数据库中运行项目下的sql文件即可自动生成，非常适合用来应付期末答辩


#### 技术说明
- 开发工具 ：IDEA2021.1.3
- 技术框架：基于Springboot构建
- 数据库：MySQL 5.6
- 项目管理工具：Maven


#### 安装教程

1.  将项目下载下来，数据库里运行sql文件即可生成数据库
2.  将application.yml文件中的数据库账号和密码修改自己数据库的账号密码
3.  为了可以不用写实体类的getter，setter方法，使用了lombok插件，需要安装一下，百度一下就行安装非常简单
4.  运行DemoApplication，浏览器输入http://localhost:8086/即可访问

#### 使用说明

-  管理员登录/注册，进入系统后可以对学生信息增删改查

#### 联系方式
-  QQ：1902013368 
-  邮箱：1902013368@qq.com

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
